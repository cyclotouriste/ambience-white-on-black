
This is the source of the White on Black Ambience for Sailfish OS.

The Ambience is on openrepos. https://openrepos.net/content/polleke/ambience-white-black

This package can be built with the Application SDK. https://sailfishos.org/wiki/Application_SDK

![Screenshot on Sailfish OS 4.0.1](assets/screenshot-20210510-001.png "Screenshot on Sailfish OS 4.0.1")
