TEMPLATE = aux
NAME=ambience-white-on-black
OTHER_FILES = \
	$${NAME}.ambience \
	images/* \
	rpm/* \
	README

ambience.files = \
	$${NAME}.ambience
ambience.path = /usr/share/ambience/$${NAME}

images.files = images/*
images.path = $${ambience.path}/images

INSTALLS += \
	ambience \
	images
