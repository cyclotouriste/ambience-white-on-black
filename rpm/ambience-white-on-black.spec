Name:       ambience-white-on-black

Summary:    White on Black Ambience
Version:    2
Release:    1
Group:      System/GUI/Other
License:    TBD
BuildArch:  noarch
Source0:    %{name}-%{version}.tar.bz2
BuildRequires:  qt5-qttools
BuildRequires:  qt5-qttools-linguist
BuildRequires:  qt5-qmake
Vendor:     Marcel Pol
Packager:   Marcel Pol
URL:        https://codeberg.org/cyclotouriste/ambience-white-on-black

Requires:   ambienced

%description
This is a White on Black Ambience.

%prep
%setup -q -n %{name}-%{version}

%build

%qmake5

make %{?jobs:-j%jobs}

%install
rm -rf %{buildroot}
%qmake5_install

%files
%defattr(-,root,root,-)
%{_datadir}/ambience/%{name}
%{_datadir}/ambience/%{name}/%{name}.ambience
# Wallpaper, should be 2048x2048
%{_datadir}/ambience/%{name}/images/white-on-black.jpg

%post
systemctl-user restart ambienced.service

%postun
systemctl-user restart ambienced.service

%changelog
* Wed Apr 26 2023 Marcel Pol <marcel@timelord.nl> 2-1
* Add separate wallpaper in correct size.

* Fri Mar 31 2023 Marcel Pol <marcel@timelord.nl> 1.1-1
* Change colors to more appropriate white or very light grey.

* Mon May 10 2021 Marcel Pol <marcel@timelord.nl> 1.0-1
- first release
